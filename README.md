**THE TKO BLOG**

**Description**

Podcasters Pdubz and Beautiful discuss and reviews all things happening in Japan from news, anime, documentary, food, you name it! This blog is made for listeners or someone interested in Japan and Japanese culture to be more well informed of current or past events happening. Posts are made weekly to keep up with current events happening.

The Home button links users to TKO Rajio's homepage for more information about the podcast. The email link is provided for those who would like to give us feedback or comment on the article which Pdubz and Beautiful may read during recording. It is also made responsive so users on the phone can experience the website without inconvenience.

With the Disqus comment plugin, users can comment by leaving GIFs, image files, links, and share through their own social media. This feature allows me to also remove irrelevant and harmful comments on my side

Users can toggle to dark mode for their convenience and share the blogposts via social media links such as Facebook, Twitter, Whatsapp, Reddit, and e-mail

**Challenges**

Of course this project is not without bugs. One challenge was it not being deployed successfully. I ensured my code is accurate and was working locally but even after waiting for some time there were no changes live. I later discovered that Netlify has failed to deploy. After finding the error code and scouting the Netlify support forum for answers, it was a simple CI Npm build fix.

Another challenge that is yet to be resolved is the Facebook Comment Plugin. It has trouble loading locally and takes some time whereas it hardly loads at all live. After several page refreshes it finally shows up. Upon clicking on the "login to comment" button, it took me to a non-stop loop of the facebook browser. After receiving no responses from Meta, I decided to find another comment plugin and discovered Disqus. There were some issues converting vanilla JS to React. After consulting people from Slack Channels and blogs I finally got it up and running.

The 'share-icon' was more simple to implement. The one strange bug is that Facebook link is the only one that does not link to the blog post whereas other social media link does. This was using the 'react-share' library.

**Tools used**

- React
- Node
- HTML
- CSS
- ButterCMS
- Axios
- Gitlab
- Webvitals
- Disqus
