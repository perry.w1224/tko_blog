import { React} from 'react';
import Chip from '../components/Chip';
import EmptyList from '../components/EmptyList';
import '../index.css';
import { Link, useParams } from 'react-router-dom';
import Disqus from "disqus-react";
// import Comments from '../components/Comments'
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  RedditShareButton,
  RedditIcon,
  WhatsappShareButton,
  WhatsappIcon,
  EmailShareButton,
  EmailIcon,
} from "react-share";


const Blog = ({ content }) => {
  const params = useParams();
  const shareURL = `https://blog.tkorajio.com/#/blog/${params.id}`;



  const createdDate = new Date(content(params.id)?.created).toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  });
  
  // const disqusShortname = 'tko-blog';
  // const disqusConfig = function () {
  //  this.page.url= `blog.tkorajio.com/#/blog/${params.id}`;
  //   this.page.identifier= `blog-${params.id}`;
  // };

  // (function() { // DON'T EDIT BELOW THIS LINE
  //   var d = document, s = d.createElement('script');
  //   s.src = 'https://blog-tkorajio-com.disqus.com/embed.js';
  //   s.setAttribute('data-timestamp', +new Date());
  //   (d.head || d.body).appendChild(s);
  //   })();



  return (
    <>    
      <Link className='blog-goBack' to='/'>
        <span> &#8592;</span> <span>Go Back</span>
      </Link>


      {content(params.id) ? (
        <div className='blog-wrap'>
          <header>
            <p className='blog-date'>Published {createdDate}</p>
            <h1>{content(params.id).title}</h1>
            <div className='blog-subCategory'>
              {content(params.id).tags && content(params.id).tags.length > 0 &&
                <div>
                  <Chip label={content(params.id).tags[0].name} />
                </div>
              }
            </div>
          </header>
          <img src={content(params.id).featured_image} alt='cover' />
          <div className='blog-content' dangerouslySetInnerHTML={{ __html: content(params.id).body }}>

          </div>
          <div className="share-button">
            <FacebookShareButton url={shareURL}>
              <FacebookIcon size={40} />
            </FacebookShareButton>

            <TwitterShareButton url={shareURL}>
              <TwitterIcon size={40} />
            </TwitterShareButton>

            <RedditShareButton url={shareURL}>
              <RedditIcon size={40} />
            </RedditShareButton>

            <WhatsappShareButton url={shareURL}>
              <WhatsappIcon size={40} />
            </WhatsappShareButton>

            <EmailShareButton url={shareURL}>
              <EmailIcon size={40} />
            </EmailShareButton>
          </div>

        </div>
      ) : (
        <EmptyList />
      )}
          {/* <Disqus.DiscussionEmbed shortname={disqusShortname} config={disqusConfig} /> */}

  </> 
  );
};
export default Blog;
