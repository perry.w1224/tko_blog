import React, { useEffect, useState } from "react";
import "../index.css";

import ToggleSwitch from "./toggleSwitch";

function Header() {
  const [theme, setTheme] = useState(localStorage.getItem("theme") || "light");


  const toggleTheme = () => {
    
    const newTheme = theme === "light" ? "dark" : "light";
    setTheme(newTheme);
    localStorage.setItem("theme", newTheme);
  };

  useEffect(() => {
    document.body.className = theme;
  }, [theme]);

  return (
    <header className="home-header">
      <a href="http://tkorajio.com" className="home">
        Home
      </a>
      <h2>TKO Rajio</h2>

      <h1>
        <i>Recommendations from our episodes</i>
      </h1>
      <p style={{fontSize: "13px"}}>Too bright? Toggle me</p>
      <div className={`App ${theme}`}>
        <ToggleSwitch onClick={toggleTheme} />
      </div>

      <p>Food, destinations, entertainment, you name it</p>
      
    </header>
  );
}

export default Header;
