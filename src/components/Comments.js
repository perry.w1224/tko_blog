import { useEffect } from 'react';

const Comments = ({ params }) => {
  const fullUrl = `https://blog.tkorajio.com/#/blog/${params.id}`;

  useEffect(() => {
    const DISQUS_SCRIPT = 'disq_script'
    const sd = document.getElementById(DISQUS_SCRIPT)
    if (!sd) {
      window.disqus_config = function() {
        this.page.url = fullUrl;
        this.page.identifier = `blog-${params.id}`;
      }
      
      const d = document
      const s = d.createElement('script')
      s.src = 'https://blog-tkorajio-com.disqus.com/embed.js'
      s.id = DISQUS_SCRIPT
      s.async = true
      s.setAttribute('data-timestamp', +new Date())

      d.body.appendChild(s)
    } else {
      if (window.DISQUS && typeof window.DISQUS.reset === 'function') {
        window.DISQUS.reset({
          reload: true,
          config: window.disqus_config,
        })
      }
    }
  }, [fullUrl, params.id])

  return <div id="disqus_thread"></div>
}

export default Comments;
