import React, { useState } from 'react';
import ReactSwitch from 'react-switch';

function ToggleSwitch(props) {
  const [checked, setChecked] = useState(false);

  const handleChange = () => {
    setChecked(!checked);
    if (props.onClick) {
      props.onClick();
    }
  }

  return (
    <ReactSwitch
      checked={checked}
      onChange={handleChange}
      uncheckedIcon={false}
      checkedIcon={false}
      onColor="#0099ff"
      offColor="#bbbbbb"
      width={40}
      height={20}
    />
  );
}

export default ToggleSwitch;
